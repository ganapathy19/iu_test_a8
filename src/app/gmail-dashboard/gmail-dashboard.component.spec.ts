import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GmailDashboardComponent } from './gmail-dashboard.component';

describe('GmailDashboardComponent', () => {
  let component: GmailDashboardComponent;
  let fixture: ComponentFixture<GmailDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GmailDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GmailDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
