import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ServiceService } from '../service.service';
import { ToastrService } from 'ngx-toastr';
import * as $ from 'jquery';

@Component({
  selector: 'app-gmail-dashboard',
  templateUrl: './gmail-dashboard.component.html',
  styleUrls: ['./gmail-dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GmailDashboardComponent implements OnInit {

  UserName: any;
  ApiUrl: String = 'gmail_list';
  RemoveUrl: String = 'gmail_remove';
  GetInboxList: any = [];
  inbox_list: any;
  showCheked = false;
  showCheked_1 = false;
  array = [];
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  SearchText: String;
  CheckAllRemoveArray: any;
  // PAGINATION
  ItemsPerPage: Number = 5;
  Page: Number = 1;
  TotalRecord: Number;
  starting_no: Number;
  ending_no: Number;
  total_page_count: Number;
  OptionList: any = [5, 10, 50];
  Option = 5;

  constructor(private formBuilder: FormBuilder, public service: ServiceService,
    private modalService: NgbModal, public Toastr: ToastrService) { }

  ngOnInit() {

    this.UserName = localStorage.getItem('user_name');
    this.getList(`${this.ApiUrl}?items_per_page=${this.ItemsPerPage}&current_page_no=${this.Page}`);

    // MULTI SELECT DROPDOWN
    this.dropdownList = [
      { item_id: 1, item_text: 'admin@gmail.com' },
      { item_id: 2, item_text: 'user@gmail.com' },
      { item_id: 3, item_text: 'employee@gmail.com' },
      { item_id: 4, item_text: 'info@gmail.com' },
      { item_id: 5, item_text: 'career@gmail.com' }
    ];
    this.selectedItems = [];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    //****************************
    // check mails
    //****************************
    $("#chkRemoveAll").change(function (e) {
      $(this).removeClass("partlyChecked");
      $(".mail-box .mail-check").prop("checked", this.checked);
      });
      $(".mail-box .mail-check").change(function (e) {
      var totalNum = $(".mail-box tr").length,
        checkedNum = $(".mail-box .mail-check:checked").length;
      if (checkedNum === 0) {
        $("#chkRemoveAll").prop("checked", false).removeClass("partlyChecked");
      } else if (totalNum === checkedNum) {
        $("#chkRemoveAll").prop("checked", true).removeClass("partlyChecked");
      } else {
        $("#chkRemoveAll").prop("checked", true).addClass("partlyChecked");
      }
    });

  }

  //  *********************************** FUNCTIONS & METHODS ***********************************
  // PAGINATION
  onChangeNextPage_1 = () => {
    if(this.current_page_no == 1) {
      this.Page = 1;
    }
    if(this.current_page_no > 1) {
      this.Page = this.current_page_no - 1;
    }
    this.getList(`${this.ApiUrl}?items_per_page=${this.ItemsPerPage}&current_page_no=${this.Page}`);
  }
  onChangeNextPage_2 = () => {
    if(this.current_page_no < this.total_page_count) {
      this.Page = Number(this.current_page_no)+Number(1);
    }
    this.getList(`${this.ApiUrl}?items_per_page=${this.ItemsPerPage}&current_page_no=${this.Page}`);
  }
  onChangeNextPage = (ev) => {
    this.Page = ev;
    this.getList(`${this.ApiUrl}?items_per_page=${this.ItemsPerPage}&current_page_no=${this.Page}`);
  }
  onChangeItemsPerPage = (ev) => {
    this.ItemsPerPage = ev.target.value;
    this.getList(`${this.ApiUrl}?items_per_page=${this.ItemsPerPage}&current_page_no=${this.Page}`);
  }
  // =====================================================================================
  // SEARCH
  onSearch = (ev) => {
    this.getList(`${this.ApiUrl}?items_per_page=${this.ItemsPerPage}&current_page_no=${this.Page}&search=${ev}`);
  }

  current_page_no: any = [];
    // ==============================================================================================================
  // GET EMPLOYEE LIST
  getList = (url) => {
    this.service.get_list(url)
      .subscribe((result) => {
        if (result.status === 'success') {
          this.GetInboxList = result.data;
          this.TotalRecord= result.total_record_count;
          this.starting_no= result.starting_no;
          this.ending_no= result.ending_no;
          this.current_page_no= result.current_page_no;
          this.total_page_count= result.total_page_count;
        } else {
          console.log('Error');
        }
      });
  }
  
  // ==============================================================================================================
   // // REMOVE SINGLE
   onRemove = (id) => {
    var obj =  {
      'id': id
    }
    this.service.post_data(this.RemoveUrl, obj)
    // this.service.get_list(`${this.RemoveUrl}?id=${id}`)
      .subscribe((result) => {
        if (result.status === 'success') {
          this.Toastr.success(result.message);
          this.getList(`${this.ApiUrl}?items_per_page=${this.ItemsPerPage}&current_page_no=${this.Page}`);
        }
      });
  }

  // ==============================================================================================================
  // // REMOVE ALL
  onRemoveAll = () => {
    var obj =  {
      'ids_array': this.array
    }
    this.service.post_data(this.RemoveUrl, obj)
    // this.service.get_list(`${this.RemoveUrl}?ids_array=${this.array}`)
      .subscribe((result) => {
        if (result.status === 'success') {
          this.Toastr.success(result.message);
          this.onChangeNextPage_1();
        }
      });
  }

  // REMOVE CHECK MULTI SELECT
  CheckAllRemove = (event, id) => {
    this.array.push(id);
    if(event.target.checked == false) {
      this.array.forEach( (currentValue, index) => {
        if(currentValue == id){
            this.array.splice(index);
        }
      });
    }
    console.log(this.array);
    if(this.array.length == 0) {
      this.showCheked = false;
    } else {
      this.showCheked = true;
    }
  }

  // REMOVE CHECK SELECT ALL
  CheckAllRemove_1 = (event) => {
    console.log(event.target.checked);
    if(event.target.checked == true) {
      this.showCheked = true;
      this.GetInboxList.forEach( (currentValue, index) => {
        this.array.push(currentValue.id);
      });
    }
    if(event.target.checked == false) {
      this.showCheked = false;
      this.array = [];
    }
  }

}
