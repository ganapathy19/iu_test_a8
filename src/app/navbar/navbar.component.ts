import { Component, OnInit } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  providers: [NgbDropdownConfig]
})
export class NavbarComponent implements OnInit {
  public sidebarOpened = false;
  toggleOffcanvas() {
    this.sidebarOpened = !this.sidebarOpened;
    if (this.sidebarOpened) {
      document.querySelector('.sidebar-offcanvas').classList.add('active');
    }
    else {
      document.querySelector('.sidebar-offcanvas').classList.remove('active');
    }
  }

  user_name: any;

  constructor(config: NgbDropdownConfig, private router: Router) {
    config.placement = 'bottom-right';

  this.user_name = localStorage.getItem('user_name');

  }
  ngOnInit() {
  }

  // =====================================================================================
  // SIGN OUT REDIRECT
  onSignOut() {
    this.router.navigate(['login']);
    this.user_name = localStorage.setItem('user_name', null);
  }

}
