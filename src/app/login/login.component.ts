import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiceService } from '../service.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, public service: ServiceService,
    public Toastr: ToastrService, private router: Router) { }

  LoginForm: FormGroup;
  submitted = false;
  LoginURL: String = 'user_login';
  Err: any;

  ngOnInit() {
    this.onCreateLoginFormInit();
  }

  onCreateLoginFormInit = () => {
    this.LoginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')] ],
      password: ['', Validators.required],
    });
  }

  get f() { return this.LoginForm.controls; }

  // =====================================================================================
  // SUBMIT
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.LoginForm.invalid) {
      return;
    }
    if (this.LoginForm.valid) {
      console.log(this.LoginForm.value);
      
      this.service.post_data(this.LoginURL, this.LoginForm.value)
        .subscribe((result) => {
          if (result.status === 'success') {
            this.onReset();
            console.log(result.data);
            localStorage.setItem('user_name', result.data.first_name);
            this.Toastr.success(result.message, result.status);
            this.router.navigate(['dashboard']);
          } else {
            this.Toastr.error(result.message, result.status);
            this.Err = Object.keys(result.errors).map(key => (result.errors[key]));
            this.Err.forEach(childObj => {
              this.Toastr.error(childObj[0], result.message);
            });
          }
        });
    }
  }

  // =====================================================================================
  // REGISTER REDIRECT
  onRegister() {
    this.router.navigate(['register']);
  }
  

  // =====================================================================================
  // FORM CLEAR
  onReset() {
    this.submitted = false;
    this.LoginForm.reset();
  }

}
