import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiceService } from '../service.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gmail-login',
  templateUrl: './gmail-login.component.html',
  styleUrls: ['./gmail-login.component.scss']
})
export class GmailLoginComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, public service: ServiceService,
    public Toastr: ToastrService, private router: Router) { }

  GloginForm: FormGroup;
  submitted = false;
  GLoginURL: String = 'user_login';
  Err: any;

  ngOnInit() {
    this.onCreateGloginFormInit();
  }

  onCreateGloginFormInit = () => {
    this.GloginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')] ],
      password: ['', Validators.required],
    });
  }

  get f() { return this.GloginForm.controls; }

  // =====================================================================================
  // SUBMIT
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.GloginForm.invalid) {
      return;
    }
    if (this.GloginForm.valid) {
      console.log(this.GloginForm.value);

      this.service.post_data(this.GLoginURL, this.GloginForm.value)
        .subscribe((result) => {
          if (result.status === 'success') {
            console.log(result.data);
            localStorage.setItem('user_name', result.data.first_name);
            this.Toastr.success('Login Successfully');
            this.router.navigate(['Gdashboard']);
            this.submitted = false;
            this.GloginForm.reset();
          } else {
            this.Toastr.error(result.message, result.status);
            this.Err = Object.keys(result.errors).map(key => (result.errors[key]));
            this.Err.forEach(childObj => {
              this.Toastr.error(childObj[0], result.message);
            });
          }
        });

    }
  }

}
