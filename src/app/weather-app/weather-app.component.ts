import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiceService } from '../service.service';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-weather-app',
  templateUrl: './weather-app.component.html',
  styleUrls: ['./weather-app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WeatherAppComponent implements OnInit {

  SearchText: String;
  ApiUrl: String = 'weather';
  API_key = '0f79bec3ae60cf35b53dfcf6f51a3bbd';
  Measure_Units = 'metric';
  Chennai = 'Chennai';
  GetAllList: any = {};
  Meaage: any;

  constructor(private formBuilder: FormBuilder, public service: ServiceService,
    public Toastr: ToastrService) { }

  ngOnInit() {
    this.GetListData(`${this.ApiUrl}?appid=${this.API_key}&q=${this.Chennai}&units=${this.Measure_Units}`);
  }

  SearchEvent() {
    this.GetListData(`${this.ApiUrl}?appid=${this.API_key}&q=${this.SearchText}&units=${this.Measure_Units}`);
  }

  // http://api.openweathermap.org/data/2.5/weather?q=Chennai&appid=0f79bec3ae60cf35b53dfcf6f51a3bbd&units=metric

   // GET WEATHER API LIST
   GetListData = (url) => {
    this.service.get_list(url)
      .subscribe((result) => {
        this.GetAllList = result;
      },
      (error) => {
        console.log(error);
        this.Toastr.error('city not found');
      });
  }

}
