import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiceService } from '../service.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-website',
  templateUrl: './website.component.html',
  styleUrls: ['./website.component.scss']
})
export class WebsiteComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, public service: ServiceService,
    public Toastr: ToastrService, private router: Router) { }

    RegisterForm: FormGroup;
    submitted = false;
    RegisterURL: String = 'user_register';
    PasswordMatch: any;
    Err: any;

    ngOnInit() {
      this.onCreateLoginFormInit();
    }
  
    onCreateLoginFormInit = () => {
      this.RegisterForm = this.formBuilder.group({
        first_name: ['', Validators.required],
        last_name: ['', Validators.required],
        email: ['', [Validators.required, Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')] ],
        mobile_no: ['', Validators.required],
        // gender: ['', Validators.required],
        address: ['', Validators.required],
        password: ['', [Validators.required, Validators.minLength(8)] ],
        cnfrm_password: ['', Validators.required],
      });
    }

    get f() { return this.RegisterForm.controls; }

  // =====================================================================================
  // SUBMIT
  onSubmit() {
    this.submitted = true;
    console.log(this.RegisterForm.value);

    // stop here if form is invalid
    if (this.RegisterForm.invalid) {
      return;
    }
    if (this.RegisterForm.valid) {

      if(this.RegisterForm.value.password != this.RegisterForm.value.cnfrm_password) {
        this.PasswordMatch = "Passwords don't match.";
          setTimeout(() => {
            this.PasswordMatch = '';
          }, 3000);
          return;
      }

      console.log(this.RegisterForm.value);
      
      this.service.post_data(this.RegisterURL, this.RegisterForm.value)
        .subscribe((result) => {
          if (result.status === 'success') {
            this.onReset();
            this.Toastr.success(result.message, result.status);
            this.router.navigate(['login']);
          }
          if (result.status === 'error') {
            this.Err = Object.keys(result.errors).map(key => (result.errors[key]));
            this.Err.forEach(childObj => {
              this.Toastr.error(childObj[0], result.message);
            });
          }
        });
    }
  }

  // =====================================================================================
  // LOGIN REDIRECT
  onLogin() {
    this.router.navigate(['login']);
  }

  // =====================================================================================
  // FORM CLEAR
  onReset() {
    this.submitted = false;
    this.RegisterForm.reset();
  }

}
