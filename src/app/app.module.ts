import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceService } from './service.service';
import { HttpClientModule } from '@angular/common/http'; 
import { ToastrModule } from 'ngx-toastr';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FilterPipe }from './filter.pipe';
// import {MultiSelectModule} from 'primeng/multiselect';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
// import { FormsComponent } from './forms/forms.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { WebsiteComponent } from './website/website.component';
import { LoginComponent } from './login/login.component';
import { GmailLoginComponent } from './gmail-login/gmail-login.component';
import { GmailDashboardComponent } from './gmail-dashboard/gmail-dashboard.component';
import { WeatherAppComponent } from './weather-app/weather-app.component';
// import { TablesComponent } from './tables/tables.component';
// import { TypographyComponent } from './typography/typography.component';
// import { IconsComponent } from './icons/icons.component';
// import { AlertsComponent } from './alerts/alerts.component';
// import { AccordionsComponent } from './accordions/accordions.component';
// import { BadgesComponent } from './badges/badges.component';
// import { ProgressbarComponent } from './progressbar/progressbar.component';
// import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
// import { PaginationComponent } from './pagination/pagination.component';
// import { DropdownComponent } from './dropdown/dropdown.component';
// import { TooltipsComponent } from './tooltips/tooltips.component';
// import { CarouselComponent } from './carousel/carousel.component';
// import { TabsComponent } from './tabs/tabs.component';
// import { CompanyInfoComponent } from './company-info/company-info.component';
// import { GalleryComponent } from './gallery/gallery.component';
// import { DonationComponent } from './donation/donation.component';
// import { WhoWeAreComponent } from './website/who-we-are/who-we-are.component';
// import { ContactComponent } from './website/contact/contact.component';
// import { FaqComponent } from './website/faq/faq.component';
// import { WebsiteDonateComponent } from './website/website-donate/website-donate.component';
// import { WhatwedoComponent } from './website/whatwedo/whatwedo.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    DashboardComponent,
    // FormsComponent,
    ButtonsComponent,
    NavbarComponent,
    WebsiteComponent,
    LoginComponent,
    GmailLoginComponent,
    GmailDashboardComponent,
    FilterPipe,
    WeatherAppComponent
    // TablesComponent,
    // TypographyComponent,
    // IconsComponent,
    // AlertsComponent,
    // AccordionsComponent,
    // BadgesComponent,
    // ProgressbarComponent,
    // BreadcrumbsComponent,
    // PaginationComponent,
    // DropdownComponent,
    // TooltipsComponent,
    // CarouselComponent,
    // TabsComponent,
    // CompanyInfoComponent,
    // GalleryComponent,
    // DonationComponent
    // WhoWeAreComponent
    // ContactComponent
    // DonationComponent,
    // FaqComponent
    // WebsiteDonateComponent
    // WhatwedoComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    FormsModule,
    NgbModule,
    CommonModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    Ng2SearchPipeModule
  ],exports:[ 
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    FormsModule,
    NgbModule,
    CommonModule,
    // BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [ServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }