import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { DashboardComponent } from './dashboard/dashboard.component';
// import { FormsComponent } from './forms/forms.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { WebsiteComponent } from './website/website.component';
import { LoginComponent } from './login/login.component';
import { GmailLoginComponent } from './gmail-login/gmail-login.component';
import { GmailDashboardComponent } from './gmail-dashboard/gmail-dashboard.component';
import { WeatherAppComponent } from './weather-app/weather-app.component';

const routes: Routes = [
  // DASHBOARD
  { path: '', redirectTo: '/WeathetApp', pathMatch: 'full' },
  { path: 'WeathetApp', component: WeatherAppComponent },
  { path: 'Glogin', component: GmailLoginComponent },
  { path: 'Gdashboard', component: GmailDashboardComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: WebsiteComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'buttons', component: ButtonsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true }), CommonModule],
  exports: [RouterModule, CommonModule]
})
export class AppRoutingModule { }
