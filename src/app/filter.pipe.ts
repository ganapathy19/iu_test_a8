
import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
name: 'SearchFilter'
})
export class FilterPipe implements PipeTransform {
    transform(GetInboxList: any[], Search: string): any[] {
        if(!GetInboxList) return [];
        if(!Search) return GetInboxList;
        Search = Search.toLowerCase();
        return GetInboxList.filter( it => {
        return it.full_name.toLowerCase().includes(Search) || it.address.toLowerCase().includes(Search) || it.interests.toLowerCase().includes(Search);
        });
    }
}
