export const environment = {
  production: true,

  apiUrl: 'http://api.openweathermap.org/data/2.5/',
  image_url: 'http://api.openweathermap.org/data/2.5/',

  // apiUrl: 'http://127.0.0.1:8000/api/',
  // image_url: 'http://127.0.0.1:8000/',

};
